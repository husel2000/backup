package it.webers.java.backup;

import it.webers.FileExt;
import it.webers.Zip;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;

//18.04.2015 - Ma.Weber - Problem mit KeepDays behoben
//21.10.2015 - Ma.Weber - Immernoch KeepDays...
public class Backup {
	
	private ArrayList<String> backupFiles;
	private ArrayList<String> excludeFiles;
	
	private String fileTargetTemplate;
	private File folderTarget;
	private ArrayList<Integer> keepDays;
	private int keepLast;
	

	public static void main(String[] args) throws Exception {
		if(args.length == 0 || args[0].replaceAll("-", "").equalsIgnoreCase("h") || args[0].replaceAll("-", "").equalsIgnoreCase("help")) {
			print_help();
			return;
		}
		
		Backup b = null;
		try {
			b = new Backup(args);
			b.start();
			b.delete();
		}catch(IllegalArgumentException e) { e.printStackTrace(); }
		catch(NullPointerException e) { e.printStackTrace(); }
	}
	
	public void delete() throws Exception {
		//Alle Dateien im Verzeichnis suchen
		File[] files = folderTarget.listFiles(new FileFilter() {
			public boolean accept(File pathname) {
				String fileName = fileTargetTemplate;
				fileName = fileName.replaceFirst("\\[\\[(.*?)\\]\\]", ".*?");
				if(pathname.getName().matches(fileName)) return true;
				return false;
			}
		});
		
		//Sortieren (neuste Index 0)
		Arrays.sort(files, new Comparator<File>(){
	    public int compare(File f1, File f2) { 
	    	return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified()); }
	    });
		ArrayList<File> filesArr = new ArrayList<File>(Arrays.asList(files));
		
		//Die letzten xxx Dateien behalten
		for(int i = 0; i < keepLast; i++) if(filesArr.size() > 0) filesArr.remove(0);
				
		//Dateien pr�fen
		for(int i = 0; i < filesArr.size(); i++) {
			File del = filesArr.get(i);
			int modifyDay = new Integer(new SimpleDateFormat("dd").format(del.lastModified()));
			for(int day : keepDays) 
				if(day == modifyDay) { System.out.println("Keep"); filesArr.remove(del); i--; }
		}		
		//Alle gefundenen Dateien löschen
		for(File del : filesArr)
			if(!del.delete()) System.out.println("Delete for File failed: " + del.getName());
	}
	
	private File getBackupFile() {
		//Ziel-Ordner und ..
		File folder = folderTarget;
		//Ziel-Datei ...
		String format = fileTargetTemplate.replaceFirst(".*\\[\\[(.*?)\\]\\].*", "$1");
		String date = new SimpleDateFormat(format).format(Calendar.getInstance().getTime());
		return new File(folder,fileTargetTemplate.replaceFirst("\\[\\[(.*?)\\]\\]", date));
	}
	
	public void start() throws Exception {
		//Prüfen
		if(backupFiles.size() == 0) throw new Exception("No Files to Backup (-include | -includelist)");
		if(fileTargetTemplate == null) throw new Exception("Filename for Backup missing (-targetFile)");
		if(folderTarget == null) throw new Exception("Folder for Backup missing (-targetName)");
		
		File backup = getBackupFile();
		//Ordner erstellen und Dateizugriff prüfen
		backup.getParentFile().mkdirs();
		if(!backup.getParentFile().exists()) throw new FileNotFoundException("Can not Create Folders for " + backup.getAbsolutePath());
		if(!backup.getParentFile().canWrite()) throw new FileNotFoundException("Permission denied on " + backup.getName());
		
		//Create Zip-Target
		Zip zout = new Zip(new FileOutputStream(backup));
		
		//Alle Includes durchgehen
		for(String file : backupFiles) {
			System.out.println(" Backup: " + file);
			FileExt inc = new FileExt(file);
			zout.addToZip(inc,getFileFilter());
		}
		zout.close();
	}	
	
	private FileFilter getFileFilter() {
		return new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				for(String regExp : excludeFiles)
					if(pathname.getAbsolutePath().matches(regExp)) return false;
				return true;
			}
		};
	}
	
	public Backup(String[] args) throws IOException {
		backupFiles = new ArrayList<>();
		keepDays = new ArrayList<>();
		excludeFiles = new ArrayList<>();
		
		for(String val : args) {
			String[] pair = val.split("=", 2);
			if(pair.length == 1) continue;
			
			switch(pair[0]) {
			case "-includeList": backupFiles.addAll(Arrays.asList(new FileExt(pair[1]).getContent().replace("\r", "").split("\n"))); break;
			case "-include": backupFiles.addAll(Arrays.asList(pair[1].split(";"))); break;
			case "-exclude": excludeFiles.addAll(Arrays.asList(pair[1].split(";"))); break;
			case "-targetFolder": folderTarget = new FileExt(pair[1]); break;
			case "-targetFile": fileTargetTemplate = pair[1]; break;
			case "-keepLast": keepLast = Integer.parseInt(pair[1]); break;
			case "-keepDay": 
				for(String day : pair[1].split(";")) {
					keepDays.add(new Integer(day));
				}
				break;
			default:
				throw new IllegalArgumentException("Parameter: " + pair[0]);
			}
		}
	}
	
	/**
	 * Schreibt den Hilfe-Text 
	 */
	public static void print_help() {
		System.out.println("");
		System.out.println("Java Backupscript");
		System.out.println("");
		System.out.println("Parameter werden nach folgender Art angegeben:");
		System.out.println("\t-parameter=wert");
		System.out.println("Sind für einen Parameter meherere Werte erlaubt ist folgendes Schema zu verwenden:");
		System.out.println("\tparameter=wert1;wert2");
		System.out.println("");
		System.out.println("Parameters:");
		System.out.println("-include - Legt fest, welche Dateien / Ordner in das Backup einbezogen werden sollen");
		System.out.println("Beispiel:");
		System.out.println("\tinclude=/home/user1/;/etc/myFile");
		System.out.println("");
		System.out.println("-includeList - Eine Datei in der jede Zeile ein Include bedeutet");
		System.out.println("Beispiel:");
		System.out.println("\tincludeList=include.conf");
		System.out.println("");
		System.out.println("-exclude - Bestimmt welche Dateien vom Backup ausgeschlossen werden sollen (WildCars .* und . erlaubt - RegExp)");
		System.out.println("Beispiel:");
		System.out.println("\texlude=/home/user1/exclude;*.exe");
		System.out.println("");
		System.out.println("-targetFolder - Ziel-Ordner für das Backup");
		System.out.println("Beispiel:");
		System.out.println("\ttargetFolder=/home/backup/backups");
		System.out.println("");
		System.out.println("-targetFile - Name des Backups. Kann Platzhalter von SimpleDateFormat verwenden.");
		System.out.println("\ttargetFile=myBackup-[[ddMMyyyy_HHmmss]].zip");
		System.out.println("");
		System.out.println("-keepLast - Gibt an, wieviele der letzten Backups behalten werden.");
		System.out.println("\tkeepLast=4");
		System.out.println("");
		System.out.println("-keepDay - Bestimmt welche Tage eines Monats behalten werden. Diese werden nicht automatisch gelöscht!");
		System.out.println("\tkeepDay=1;10;20");
		
	}
}
